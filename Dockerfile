FROM ubuntu:16.04
MAINTAINER fahimx
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -yq curl apache2 php libapache2-mod-php php-mysql libaio1 libnuma1 mysql-client mysql-client-5.7 mysql-client-core-5.7 mysql-common && apt-get clean &&  rm -rf /var/lib/apt/lists/*  
EXPOSE 80 443
WORKDIR /var/www/html
CMD ["apache2ctl", "-D", "FOREGROUND"]
